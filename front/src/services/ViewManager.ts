/* eslint-disable */
import router from '../router'
import io from 'socket.io-client'
import * as config from '../../../config.js'

interface IViewManager {
  changeView(): void
  checkStatus(): void
  stopCheckingStatus(): void
}
type SetIntervalType = ReturnType<typeof setInterval>
type SocketIoType = ReturnType<typeof io> // definiujemy typ dla socketu

class ViewManager implements IViewManager {
  private interval: undefined | SetIntervalType
  private status = ''
  private socket: undefined | SocketIoType


  changeView() {
    switch (this.status) {
      case 'CONNECTED':
        router.push({ name: 'connected' })
        break
      case 'FAILED':
        router.push({ name: 'failed' })
        break
      case 'ANSWERED':
        router.push({ name: 'answered' })
    }

    
  
  }
  checkStatus() {
    this.socket = io(config.api.url, {          
        reconnection: false,
        
        transports: ["websocket", "polling"],
        				
      });
    this.socket.on('status', (status) => {
        console.log(status)
        this.status = status
        this.changeView()
    })
  }
  stopCheckingStatus() {
    this.socket?.close()
  }


}

export default new ViewManager()