import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import startView from '../views/startView.vue'
import RingingView from '../views/RingingView.vue'
import Connected from '../views/ConnectedView.vue'
import Failed from '../views/FailedView.vue'
import Answered from '../views/AnsweredView.vue'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'start',
    component: startView
  },
  {
    path: '/',
    name: 'ringing',
    component: RingingView
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    
  },
  {
    path: '/',
    name: 'failed',
    component: Failed,
  },
  {
    path: '/',
    name: 'connected',
    component: Connected,
  },
  {
    path: '/',
    name: 'answered',
    component: Answered,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
