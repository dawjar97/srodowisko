const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const { Server } = require('socket.io');
const config = require('./config');

const server = app.listen(3000, () => { // przypisujemy instancje serwera express do zmiennej 
   console.log('app listening on port 3000');
});



const io = new Server(server);

 io.on("connection", (socket) => { // nasłuchujemy na rozpoczęcie połączenia
     console.log('Połączono socket');
     io.emit("status", 5555);
   });

Dialer.configure(config.dialer);
app.use(cors());
app.use(bodyParser.json());

let bridge = null;
app.get('/call/:number1/:number2',async (req, res) => {
   const number1 = req.params.number1;
   const number2 = req.params.number2;
   console.log(number1,number2)
   bridge = await Dialer.call(number1,number2).catch(err=>{console.log(err)});
   res.json({success: true});
})

app.get('/status', async (req, res) => {
   let status = 'NONE';
 if (bridge !== null) {
   status = await bridge.getStatus();
 }
  res.json({success: true, status: status});
});

app.post('/call/',async (req, res) => {
   const body = req.body;
   
   
   const number1 =  body.number;
   const number2 =  config.agent_number; 
    

   bridge = await Dialer.call(number1,number2).catch(err=>{console.log(err)});
   console.log('body',body);
   
   let oldStatus = null
   let interval = setInterval(async () => {
      let currentStatus = await bridge.getStatus();
      if (currentStatus !== oldStatus) {
         oldStatus = currentStatus
         io.emit('status', currentStatus)
      }
      if (currentStatus === 'ANSWERED') {
         clearInterval(interval)
      }
   }, 1000)
   
   
   res.json({ success: true });
  })